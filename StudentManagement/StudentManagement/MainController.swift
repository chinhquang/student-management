//
//  ViewController.swift
//  StudentManagement
//
//  Created by Night Dog on 10/14/18.
//  Copyright © 2018 CHINHQUANG. All rights reserved.
//

import UIKit

class MainController: UIViewController {
    static var list: [StudentInfo] = []
    var itemTemp = StudentInfo();
    @IBOutlet weak var editBTN: UIBarButtonItem!
    @IBOutlet weak var tbview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbview.delegate = self
        self.tbview.dataSource =  self
        // Do any additional setup after loading the view, typically from a nib.
        
        
    
    }
    
    @IBAction func editAction(_ sender: UIBarButtonItem) {
        self.tbview.isEditing = !self.tbview.isEditing
        sender.title = (self.tbview.isEditing) ? "Done" : "Edit";
    }
}
extension MainController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MainController.list.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let student_item = MainController.list[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentInfoCell") as! StudentInfoCell
        cell.setCells(info: student_item);
        return cell;
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true;
        
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            MainController.list.remove(at: indexPath.row);
            tbview.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade)
        }else if editingStyle == .insert{
            
        }
    }
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true;
    }
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = MainController.list[sourceIndexPath.item];
        MainController.list.remove(at: sourceIndexPath.item)
        MainController.list.insert(item, at: destinationIndexPath.item);
    }
    
}
