//
//  StudentInfoCell.swift
//  StudentManagement
//
//  Created by Night Dog on 10/16/18.
//  Copyright © 2018 CHINHQUANG. All rights reserved.
//

import Foundation
import UIKit
class StudentInfoCell : UITableViewCell{
    
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtBirthday: UILabel!
    @IBOutlet weak var txtClassName: UILabel!
    @IBOutlet weak var txtOthers: UILabel!
    @IBOutlet weak var txtGender: UILabel!
    func setCells(info : StudentInfo){
        imgAva.image = info.image;
        txtName.text = "Fullname: " + info.name
        txtBirthday.text = "Birthday: " + info.birth
        txtGender.text = "Gender: " + info.gender
        txtClassName.text = "Class: " + info.classname
        txtOthers.text = "Others: " + info.others
    }
}
