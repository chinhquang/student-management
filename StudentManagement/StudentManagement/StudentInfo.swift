//
//  StudentInfo.swift
//  StudentManagement
//
//  Created by Night Dog on 10/16/18.
//  Copyright © 2018 CHINHQUANG. All rights reserved.
//

import Foundation
import UIKit
class StudentInfo{
    var image : UIImage
    var name : String
    var classname : String
    var birth : String
    var others : String
    var gender : String
    init (image : UIImage, name : String,classname : String,birth : String,others : String,gender : String){
        self.image = image;
        self.name = name ;
        self.classname = classname;
        self.birth = birth;
        self.others = others;
        self.gender = gender;
    }
    init (){
        self.image = UIImage();
        self.name = "";
        self.classname = "";
        self.birth = "";
        self.others = "";
        self.gender = "";
    }
}
