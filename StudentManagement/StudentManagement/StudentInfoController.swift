//
//  StudentInfoController.swift
//  StudentManagement
//
//  Created by Night Dog on 10/15/18.
//  Copyright © 2018 CHINHQUANG. All rights reserved.
//

import UIKit
class StudentInfoController : UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    var pickerData: [String] = [String]();
    var indexGender = 0 ;
    @IBOutlet weak var datePicker_Birth: UIDatePicker!
    @IBOutlet weak var txtClass: UITextField!
    @IBOutlet weak var GenderPicker: UIPickerView!
    @IBOutlet weak var txtOther: UITextField!
    override func viewDidLoad() {
        super .viewDidLoad();
        self.GenderPicker.delegate = self
        self.GenderPicker.dataSource = self
        
        UI.addDoneButton(controls: [txtName,txtClass,txtOther])
        registerKeyboardNotification();
        
        // Input the data into the array
        pickerData = ["Female","Male"]
        
        
    }
    //----------------------------------------------------------------------------------------------------
    override func viewDidDisappear(_ animated: Bool) {
        super .viewDidDisappear(animated)
        deregidterKeyboardNotification()
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        scrollview.isScrollEnabled = true
        let info = notification.userInfo;
        let keyboard = (info?[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
        let contentInset = UIEdgeInsets(top: 0.0,left: 0.0,bottom: keyboard.height,right: 0.0);
        self.scrollview.contentInset = contentInset;
        self.scrollview.scrollIndicatorInsets = contentInset;
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        
        let info = notification.userInfo;
        let keyboard = (info?[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
        let contentInset = UIEdgeInsets(top: 0.0,left: 0.0,bottom: -keyboard.height,right: 0.0);
        self.scrollview.contentInset = contentInset;
        self.scrollview.scrollIndicatorInsets = contentInset;
    }
    func registerKeyboardNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func deregidterKeyboardNotification(){
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillShowNotification,object : nil)
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillHideNotification,object : nil)
    }
    //----------------------------------------------------------------------------------------------------
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        indexGender = row;
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    func getInfo ()->StudentInfo{
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        let strDate = dateFormatter.string(from: datePicker_Birth.date)
        
        return StudentInfo(image: imgAva.image!, name: txtName.text!, classname: txtClass.text!, birth: strDate, others: txtOther.text!, gender: pickerData[indexGender]);
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueShowTable"{
            MainController.list.append(getInfo());
            
        }
    }
    
    
}

