//
//  Keyboard.swift
//  StudentManagement
//
//  Created by Night Dog on 10/15/18.
//  Copyright © 2018 CHINHQUANG. All rights reserved.
//


import UIKit
class UI{
    static func addDoneButton (controls : [UITextField]){
        for textField in controls {
            let toolbar = UIToolbar();
            toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target:textField, action: #selector(UITextField.resignFirstResponder))];
            toolbar.sizeToFit();
            textField.inputAccessoryView = toolbar;
        }
        
    }
}

